 # BaseML CommonMark Markdown

 https://baseml.soilsrc.org
 
 This a paragraph with sentances that do not have a hard line returns. Notice this is all on the same line.

Here is a new paragraph with a blank line between.

Headings are level one,two, and threee depending on how many Octothorpes. 

## Formating

*One star for italics*

**Two Stars for bold**
 
 ***Three stars for bold italics.***

 Dont mix formatting even though you somtimes can.

 ## Lists

 * This
 * is 
 * an 
 * unodered 
 * list

  Just stick with one level.

  1. This
  1. Is 
  1. An
  1. ordered
  1. list.

  ## Blocks 

  > 😊 Emojis can add content

  ## Fences

  A fence bost is three backticks.

  ```
  Roses are Red
  Vilots are blue?
  Or are they
  ```
  You can add a laguage tag.

  ```js
  console.log('Hello World')
  ```

  Also you can use tilde (same key just Shift) instead 
  
  ~~~md
  This has some backticks in it

  ``` 
  Roses
  ```
  ~~~

  Backtick is main option. Tilde is backup.

## Links

https://skilstak.io

<dylanhoule@gmail.com>

<tel:555-555-5555>

Or you can use an [inline link](hhtps://skilstak.io)

## Separators 

Somthimes called line or horizontal rules but ofter they aer not rendered as lines so call them separators.

----

-----

-----

Always make sure there is a blank line between them.

## Images 

Images are just links with an exclamation point (bang) in front.

![Hammond Dance](./HammondDefualtDance.gif)
  
You can also link from an image.

[![Hammond Dance](./HammondDefualtDance.gif)](https:google.com)