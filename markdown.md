# Level One 

*Lorem* ipsum **dolor** sit ***amet***, consectetur adipiscing elit. Immo alio genere; Nescio quo modo praetervolavit oratio. Quid censes in Latino fore? Tu quidem reddes;

# Level Two

`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Immo alio genere; Nescio quo modo praetervolavit oratio. Quid censes in Latino fore? Tu quidem reddes;`
Here is [freecodecamp](https://freecodecamp.org). Also <https://codecombat.com> is a good tool. For more information you can contact me at <mailto:fakeemail@fakeemail.com>
or phone me at <tel:555-555-8888> 
[![freecodecamp](https://upload.wikimedia.org/wikipedia/commons/3/39/FreeCodeCamp_logo.png)](https://freecodecamp.org)

# Reasons to learn Markdown
* Easy
* Helpful
 
1. Easy 
1. Helpful

---

Roses are red  
Violets are blue
 
``` print("hello world")```

> Line Block 1
>
> Line Block 1

> ❤️  You can use emojis aswell.


