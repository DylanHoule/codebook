# ***CSS Color Style***

At top of code remember the ```<style> </style>```   Inside of the style tag, you can further specify what you want the style to go to, You can do this with style classes.
This is the better option. You can call these classes back at any time.  ``` <style>
  .blue-text {
    color: blue;
  }
</style>``` 
. You can recall these classes at any time by useing ```<h2 class="blue-text">CatPhotoApp</h2> ```  Or ```<img class="fixed-image" src="http://www.example.com/picture"/> ```
Remember to make sure that if you are adding more make sure to use
``` h1 {color : red;} ``` inside of the style tag.

# ***CSS  Font Style***
 
 You can change the font size of a text by using ``` .font-16 {
  font-size: 16px; } ``` That would be what you would do for a class for the font size 16px.
  
  You can change the font style by using ``` h2 {
  font-family: monospace; }``` If you wanted the font to be monospace. This code above would change all of the header2's to the font monospace.
If you did not want this you could use a class. For example ``` .monospace {font-family : monospace;} ``` 

You can export a google font by using ```<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css"> ```Before your style tag. 
If you wanted to use the lobster font.

You can also apply backup fonts by simply adding the name of the backup font(s) to the end of the first selected font.

You can add a border around an image by using ```<style>
  .thin-red-border {
    border-color: red;
    border-width: 5px;
    border-style: solid;
  }
</style>``` 

You can seperate and apply different classes to one element by puting a space inbetween. For example you can do ```<h1 style:"blue-text green-border">```

































# TO FIND IP  DO ifconfig #