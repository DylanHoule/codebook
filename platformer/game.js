const keys = 'UP,DOWN,LEFT,RIGHT,SPACE,W,A,S,D,R'
let pl, k, score, background, bump, coin, jump, start

const randint = lim => Math.floor(Math.random() * lim)
const randrng = (min,max) => randint(max-min+1) + min
const rX = () => randint(game.config.width)
const rY = () => randint(game.config.height)


class Main extends Phaser.Scene {
    init() {
        score = 0
        let hscore = localStorage('hscore')
    }
    
    
    preload() {
        this.load.image('bg', "./assets/img/background.png")
        this.load.image('pl', "./assets/img/bread.png")
        this.load.image('platform', "./assets/img/platform.png")
        this.load.image('enemy', "./assets/img/enemy.png")
        this.load.image('coin', "./assets/img/coin.png")
        this.load.image('gameover', "./assets/img/gameover.png")

        this.load.audio('bump', './assets/snd/bump.mp3')
        this.load.audio('jump', './assets/snd/jump.mp3')
        this.load.audio('background', './assets/snd/background.wav')
        this.load.audio('pickup', './assets/snd/coin.wav')
        this.load.audio('diesound', './assets/snd/dead.wav')
        

    }


    create() {


        this.add.image(0, 0, 'bg').setOrigin(0, 0)
        let diesound =  this.sound.add('diesound')
        let bump = this.sound.add('bump')
        let jump = this.sound.add('jump')
        let pickup = this.sound.add('pickup')
        background = this.sound.add('background')
        background.play()
        pl = this.physics.add.sprite(110, 110, 'pl')
        .setCollideWorldBounds(true)
        .setScale(2)
        .setGravityY(1200)

        let bad = this.physics.add.sprite(110, 110, 'enemy')
        .setCollideWorldBounds(true)
        .setScale(2)

        .setBounce(1)
        .setVelocity(300)

        k = this.input.keyboard.addKeys(keys)

        let platform = this.physics.add.staticGroup()
        const SpawnPlatform = (x,y) => platform.create(rX(), rY(), "platform")
        //platform.create(300, 300, 'platform')
        //platform.create(200, 200, 'platform')
      
        let coins = this.physics.add.staticGroup()

        const spawnCoin = (x,y) => coins.create(rX(), rY(), "coin")
            
        

        for (let i=0; i<1; i++) spawnCoin() 

        let scoreText = this.add.text(16, 16, "Score: 0", {
            color: "grey",
            fontFamily: "Comic Sans MS",
            fontSize: "30px"
        })

        const collect = (p,c) => {
            score += 1
            scoreText.setText(`Score: ${score}`)
            pickup.play()
            c.destroy()
            spawnCoin()
            SpawnPlatform()

            
        
        }
        const collideBad = (p,c) => {
            this.physics.pause()
            background.stop()
            diesound.play()
            this.add.image(0,0, 'gameover').setOrigin(0,0) 
            p.setTint('#FF0000')
        }

        this.physics.add.collider(pl, bad, collideBad)
        this.physics.add.collider(bad, platform)
        this.physics.add.overlap(pl, coins, collect)
        this.physics.add.collider(pl, platform)
        

         
    }

    update() {
        if (k.LEFT.isDown || k.A.isDown) {
            pl.setVelocityX(-400)
        }

        else if (k.RIGHT.isDown || k.D.isDown) {
            pl.setVelocityX(400)
 
        }

        // if (pl.body.onFloor()) {
        pl.setDragX(1600)

        if (k.UP.isDown || k.SPACE.isDown || k.W.isDown) {
            pl.setVelocityY(-400)
        
        }

        if (k.R.isDown) {
            this.scene.restart()
            background.stop()
            return
        }
    }

}


let game = new Phaser.Game({
    scene: Main,
    physics: { default: 'arcade' },
    width: 1024,
    height: 768,
    pixelArt: true
     
    
})